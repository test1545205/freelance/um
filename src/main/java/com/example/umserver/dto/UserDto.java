package com.example.umserver.dto;

import com.example.umserver.entity.RoleEntity;
import com.example.umserver.entity.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;

public class UserDto {

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Save{
        private UserEntity user;
        private Collection<RoleEntity> roles;
    }
}
