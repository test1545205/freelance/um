package com.example.umserver.controller;

import com.example.umserver.dto.UserDto;
import com.example.umserver.entity.UserEntity;
import com.example.umserver.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/um")
public class UserController {

    private final UserService userService;

    @GetMapping
    public ResponseEntity<List<UserEntity>> listUser(){
        return ResponseEntity.ok(userService.listUser());
    }

    @GetMapping("/find-by-email")
    public ResponseEntity<UserEntity> findUserByEmail(
            @RequestParam(name = "email") String email
    ){
        return ResponseEntity.ok(userService.findByEmail(email));
    }

    @PostMapping
    public ResponseEntity<UserEntity> save(@RequestBody UserDto.Save data){
        return ResponseEntity.ok(userService.save(data));
    }

}
