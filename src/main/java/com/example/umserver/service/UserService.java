package com.example.umserver.service;

import com.example.umserver.dto.UserDto;
import com.example.umserver.entity.RoleEntity;
import com.example.umserver.entity.UserEntity;
import com.example.umserver.exceptions.IdNotFoundException;
import com.example.umserver.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    public UserEntity save(UserDto.Save data){

        UserEntity userEntity = data.getUser();
        Collection<RoleEntity> roles = data.getRoles();

        UserEntity entity = new UserEntity(null, userEntity.getFirstName(), userEntity.getLastName(), userEntity.getEmail(), userEntity.getPassword(), roles);
        return userRepository.save(entity);
    }

    public List<UserEntity> listUser(){
        return userRepository.findAll();
    }

    public UserEntity findByEmail(String email){
        return userRepository.findByEmail(email).orElseThrow(() ->
                new IdNotFoundException("User dengan email " + email + " tidak ditemukan"));
    }

}
