package com.example.umserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UmServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(UmServerApplication.class, args);
	}

}
